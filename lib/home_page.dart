import 'package:bmi_calculator/utils/bmi_util.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextEditingController weightController = TextEditingController();
  final TextEditingController heightController = TextEditingController();
  String message = '';

  void _calculateUserBMI() {
    final bmi = calculateBMI(
      weight: int.parse(weightController.text),
      height: int.parse(heightController.text),
    );
    setState(() {
      if (bmi < 18.6) {
        message = 'You are under weight';
      } else if (bmi >= 18.6 && bmi < 24.9) {
        message = 'You\'re weight is Normal';
      } else if (bmi >= 25.0 && bmi < 29.9) {
        message = 'You are overweight';
      } else {
        message = 'You are obese';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final buttonStyle = ElevatedButton.styleFrom(
      textStyle: const TextStyle(
        fontSize: 24.0,
      ),
      padding: const EdgeInsets.all(16.0),
    );

    return Container(
      margin: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          TextField(
            controller: weightController,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Enter Weight (kg)',
            ),
          ),
          const SizedBox(
            height: 16.0,
          ),
          TextField(
            controller: heightController,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Enter Height (meters)',
            ),
          ),
          const SizedBox(
            height: 16.0,
          ),
          ElevatedButton(
            style: buttonStyle,
            onPressed: _calculateUserBMI,
            child: const Text('Calculate BMI'),
          ),
          const SizedBox(
            height: 16.0,
          ),
          Text(
            message,
            style: const TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
